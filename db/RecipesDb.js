const csv = require('csvtojson');

class RecipesDb {
  constructor() {
    this.recipes = null;
  }

  async init() {
    const recipeArray = await csv({
      checkType: true
    }).fromFile('./db/recipe-data.csv');
    this.recipes = new Map(recipeArray.map(r => [r.id, r]));
  }

  async getById(id) {
    if (this.recipes === null) {
      await this.init();
    }
    return this.recipes.get(parseInt(id));
  }

  async list(page, pageSize) {
    if (this.recipes === null) {
      await this.init();
    }

    const startIndex = (page - 1) * pageSize;
    const endIndex = page * pageSize - 1;

    const recipes = Array.from(this.recipes.values()).slice(
      startIndex,
      endIndex + 1
    );
    return recipes.map(r => ({
      id: r.id,
      title: r.title,
      marketing_description: r.marketing_description
    }));
  }

  async put(id, recipe) {
    if (this.recipes === null) {
      await this.init();
    }

    this.recipes.set(parseInt(id), recipe);
  }
}

module.exports = RecipesDb;
