const DEFAUTL_PAGE_SIZE = 10;

class RecipesController {
  async getById(ctx) {
    const db = ctx.deps.db;

    const recipe = await db.getById(ctx.params.id);
    if (!recipe) {
      ctx.status = 404;
      return;
    }

    ctx.status = 200;
    ctx.body = recipe;
  }

  async list(ctx) {
    const db = ctx.deps.db;

    const page = ctx.request.query.page || 1;
    const pageSize = ctx.request.query.pageSize || DEFAUTL_PAGE_SIZE;

    ctx.status = 200;
    ctx.body = await db.list(page, pageSize);
  }

  async put(ctx) {
    const db = ctx.deps.db;
    const { id } = ctx.params;
    const recipe = ctx.request.body;
    await db.put(id, recipe);
    ctx.status = 200;
  }
}

module.exports = RecipesController;
