const Koa = require('koa');
const Router = require('koa-router');
const bodyParser = require('koa-bodyparser');
const cors = require('@koa/cors');
const Db = require('./db/RecipesDb');
const koadepsi = require('koa-depsi');

const PORT = 3000;

const app = new Koa();

app.use(bodyParser());

app.use(cors());

// error handling
app.use(async (ctx, next) => {
  try {
    await next();
  } catch (err) {
    ctx.status = err.status || 500;
    ctx.body = err.message;
    ctx.app.emit('error', err, ctx);
  }
});

const db = new Db();
app.use(
  koadepsi({
    db
  })
);

// instantiate our new Router
const router = new Router();
require('./routes/recipes')({ router });

app.use(router.routes());
app.use(router.allowedMethods());

// Start server
const server = app.listen(PORT);
module.exports = server;
