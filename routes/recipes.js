const RecipesController = require('../controllers/RecipesController');

module.exports = ({ router }) => {
  controller = new RecipesController();

  router.get('/recipes', ctx => controller.list(ctx));
  router.get('/recipes/:id', ctx => controller.getById(ctx));
  router.put('/recipes/:id', ctx => controller.put(ctx));
};
