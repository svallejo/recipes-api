# Recipes REST API

This project requires Node.js to run. To install the project dependencies:

```
yarn install
```

## Run

```
yarn run start
```

By default it will start the server on the port `3000`. It could be changed on the `app.js` file.

## Tests

This project has got automated tests.

```
yarn run test
```

## About the solution

All the three use cases were implemented in the solution. These are the endpoints:

- GET /recipes?page=X
- GET /recipes/id
- PUT /recipes/id

The recipes are loaded from the file `db/recipe-data.csv`. To simulate the database I used Javascript Maps. The class RecipesDb is injected to the controller so it could be easily replaced with a real database access class that implements the same interface.

## Possible improvements and functionality

The first improvement would be finishing the operations related with the recipes, adding the POST and DELETE methods.
