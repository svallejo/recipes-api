const request = require('supertest');
const server = require('../app.js');

// close the server after each test
afterAll(() => {
  server.close();
});

describe('Recipes by id', () => {
  test('Existing recipe', async () => {
    const response = await request(server).get('/recipes/1');
    expect(response.status).toEqual(200);
    expect(response.body.title).toEqual(
      'Sweet Chilli and Lime Beef on a Crunchy Fresh Noodle Salad'
    );
  });
  test('Non existing recipe', async () => {
    const response = await request(server).get('/recipes/1000');
    expect(response.status).toEqual(404);
  });
});

describe('Recipes paginated list', () => {
  test('First page', async () => {
    const response = await request(server).get('/recipes?pageSize=3');
    expect(response.body.length).toEqual(3);
    expect(response.body[0].id).toEqual(1);
  });
  test('Second page', async () => {
    const response = await request(server).get('/recipes?page=2&pageSize=3');
    expect(response.body.length).toEqual(3);
    expect(response.body[0].id).toEqual(4);
  });
  test('Empty page', async () => {
    const response = await request(server).get('/recipes?page=1000');
    expect(response.body).toEqual([]);
  });
});

describe('Update recipe', () => {
  test('Modify recipe 1', async () => {
    let { body } = await request(server).get('/recipes/1');
    body.title = 'Modified title';

    const putResponse = await request(server)
      .put('/recipes/1')
      .set('Accept', 'application/json')
      .send(body);
    expect(putResponse.status).toEqual(200);

    const response = await request(server).get('/recipes/1');
    expect(response.status).toEqual(200);
    expect(response.body).toEqual(body);
  });
});
